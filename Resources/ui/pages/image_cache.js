GenericImageCache = {};
 
GenericImageCache.on = false;                // enable/disable cache functionality
GenericImageCache.SUB_DIR_NAME = "yb/";
GenericImageCache.DIRECTORY = Titanium.Filesystem.applicationDataDirectory + GenericImageCache.SUB_DIR_NAME;
 
/**
 * Is our image cached? Easy to check. Just look for the file
 * and if it's there - we've got our answer!
 * @param {Object} name
 */
GenericImageCache.isCached = function (name)
{
    if (!GenericImageCache.on) {
        return false;
    }
 
    Ti.API.info ("[image_cache.js] Looking for profile file named: " + (GenericImageCache.DIRECTORY + name) + "...");
    var f = Titanium.Filesystem.getFile(GenericImageCache.DIRECTORY, name);
    return f.exists();
};
 
/**
 * Return File object to our cached file
 * 
 * @param {Object} sender_guid
 */
GenericImageCache.getFile = function (name)
{
    var f = Titanium.Filesystem.getFile(GenericImageCache.DIRECTORY, name);
    return f;
};
 
/**
 * Return the nativePath to our cached file, which can be used
 * in the url item for an imageView
 * 
 * @param {Object} sender_guid
 */
GenericImageCache.getNativeURL = function (name)
{
    var f = GenericImageCache.getFile (name);
    return f.nativePath;
};
 
/**
 * Allow users to specify a subdirectory to be created and used off
 * applicationDataDirectory. By default this class will use "gic" for GenericImageCache
 */
GenericImageCache.setDirectory = function (name)
{
    var dir_name = (name.charAt (name.length - 1) != '/') ? (name + '/') : name;
 
    GenericImageCache.SUB_DIR_NAME = dir_name;
    GenericImageCache.DIRECTORY = Titanium.Filesystem.applicationDataDirectory + GenericImageCache.SUB_DIR_NAME;
};
 
/**
 * Does our sub directory we want to use exist?
 */
GenericImageCache.directoryExists = function ()
{
    var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,GenericImageCache.SUB_DIR_NAME);
    var rc = dir.exists();
 
    Ti.API.info ("image_cache.js:directoryExists " + rc);
 
    return rc;
};
 
/**
 * Create our directory if it doesn't alerady exist
 */
GenericImageCache.createDirectory = function ()
{
    var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,GenericImageCache.SUB_DIR_NAME);
    var rc = dir.createDirectory();
 
    Ti.API.info ("image_cache.js:createDirectory " + rc);
 
    return rc;
};
 
/**
 * Save our blob image to cache by writing it to disk
 * 
 * @param {Object} sender_guid
 * @param {Object} blob
 */
GenericImageCache.save = function (name, blob)
{
    if (!GenericImageCache.on) {
        return;
    }
 
    if (!GenericImageCache.directoryExists()) {
        GenericImageCache.createDirectory ();
    }
 
    Ti.API.info ("[image_cache.js:saveImage] Saving " + blob.length + " bytes to " + name + "...");
    var f = Titanium.Filesystem.getFile(GenericImageCache.DIRECTORY, name);
    f.write (blob);
};
 
/**
 * Enumerate the cache directory. Return an array of file names
 */
GenericImageCache.enumDirectory = function ()
{
    var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,GenericImageCache.SUB_DIR_NAME);
    var listing = dir.getDirectoryListing();
    return listing;
};
 
/**
 * Return the number of items in our cache directory
 */
GenericImageCache.itemCount = function ()
{
    return GenericImageCache.enumDirectory().length;
};
 
/**
 * Return the total number of bytes in our cache
 */
GenericImageCache.totalBytes = function ()
{
    var flist = GenericImageCache.enumDirectory();
    var f;
    var total_bytes = 0;
 
    for (var i = 0; i < flist.length; i++) {
        f = Titanium.Filesystem.getFile(GenericImageCache.DIRECTORY, flist[i]);
        if (f.exists() && (f.size > 0)) {
            total_bytes += f.size;
        }
    }
 
    return total_bytes;
};