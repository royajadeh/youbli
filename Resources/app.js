
		
// Custom Tab Group sample

// Have a good reading!
//  -- Yury Skaletskiy, yury.skaletskiy@gmail.com

// include control JS

 Ti.include("control_CustomTabGroup.js");

// include tab contents
Ti.include("/tab1view.js");
Ti.include("/tab2view.js");
Ti.include("/tab3view.js");
Ti.include("/tab4view.js");
Ti.include("/tab5view.js");


// calculate window independed icon size (we store 64x64 icons in this sample)

var ratio480 = Ti.Platform.displayCaps.platformWidth / 480; 
var iconWH = 64 * ratio480;


// tab group configuraiton goes here
// create full-screen tab group container


var tabGroup = control_createCustomTabGroup({
	tabs: [ 
		{
			name: "menu",
		//	title: "Category",
			
			imagePath: "/images/ic_browse.png",
			selectedImagePath: "/images/ic_browse.png",
			imageWidth: iconWH,
			imageHeight: iconWH,
			
			view: tab1view,
		},
		
		{ 
			name: 'specialactions',
		//	title: "Search",
			imagePath: "/images/ic_search.png",
			selectedImagePath: "/images/ic_search.png",
			imageWidth: iconWH,
			imageHeight: iconWH,
			
			view: tab2view,

		},
		{
			name: "cart",
			//title: "ShootNsell",
			imagePath: "/images/ic_shoot.png",
			selectedImagePath:  "/images/ic_shoot.png",
			imageWidth: iconWH,
			imageHeight: iconWH,
			
			view: tab3view,
		},
		{
			name: "order",
		//	title: "Messages",
			imagePath: "/images/ic_message.png",
			selectedImagePath: "/images/ic_message.png",
			imageWidth: iconWH,
			imageHeight: iconWH,
			
			view: tab4view,
		},
		{
			name: "profile",
			//title: "MyBli",
			imagePath: "/images/ic_profile.png",
			selectedImagePath: "/images/ic_profile.png",
			imageWidth: iconWH,
			imageHeight: iconWH,
			
			view: tab5view,
		}
		
		
	],
	
	
	tabClick: function(e) {
		
		Ti.API.info("* tabGroup click on tab " + e.index);
		// switch between tabs
		tabGroup.setActiveTab(e.index);		
		tabGroup.setTabBadge("order", e.index)
	},
	
});

// create main window and show
var mainWindow = Ti.UI.createWindow({
	height:Ti.UI.FILL,
	backgroundColor: '#fff',
	tabBarHidden:true,	
	navBarHidden:true,	
	statusBarHidden:true,
	exitOnClose: true,
	fullscreen:true,
	orientationModes: [ // at this time, it works only on portrait screens
		Titanium.UI.UPSIDE_PORTRAIT,
		Titanium.UI.PORTRAIT,
	],
	
	modal:true,
});
var bg = Ti.UI.createView({
    top: 0,
    width: '100%',
    height: 40,
    zIndex:204,
    shadowColor:'#000',
    shadowOffset:{x:5,y:5},
    shadowRadius:6,
    backgroundGradient: {
        type: 'linear',
        startPoint: { x: '0%', y: '0%' },
        endPoint: { x: '0%', y: '100%' },
        colors: [ { color: '#f2f2f2', offset: 0.0}, { color: '#fff', offset: 0.50 }, { color: '#ccc', offset: 1.0 } ],
    }
});
var bg1 = Ti.UI.createView({
	backgroundColor: '#999',
	width: '100%',
	height: 40,
	layout:'center',
	top:0.7,
	zIndex:203
});
var bg2 = Ti.UI.createView({
	backgroundColor: '#ccc',
	width: '100%',
	height: 40,
	layout:'center',
	top:1.4,
	zIndex:202
});
var headlg = Ti.UI.createImageView({
	image : '/images/head_youbli.png',
	//width: '44dp',
	height: 30,
	layout:'center',
	top:5,
	zIndex:205
});
var topbg = Ti.UI.createImageView({	
	backgroundColor: '#38a59a',
	width: '100%',
	height:40,
	layout:'center',
	top:40,
	opacity:0.8,
	zIndex:100
});
var judul = Ti.UI.createLabel({
	font:{fontSize:14, fontWeight:'bold'},
	color:'#fff',
	height:'auto',
	text:'Recent',
	top:50,
	layout:'center',
	textAlign:'center',
	zIndex:101
});	

mainWindow.add(tabGroup.root)
mainWindow.add(bg)
mainWindow.add(bg1)
mainWindow.add(bg2)
mainWindow.add(headlg)
//mainWindow.add(topbg)
//mainWindow.add(judul)
mainWindow.open();
